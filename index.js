setInterval(setClock, 1000)

const hourHand = document.querySelector('[data-hour-hand]')
const minuteHand = document.querySelector('[data-minute-hand]')
const secondHand = document.querySelector('[data-second-hand]')

function setClock() {

  const currentDate = new Date()
  const secondsRatio = currentDate.getSeconds() / 60
  const minutesRatio = (secondsRatio + currentDate.getMinutes()) / 60
  const hoursRatio = (minutesRatio + currentDate.getHours()) / 12
  setRotation(secondHand, secondsRatio)
  setRotation(minuteHand, minutesRatio)
  setRotation(hourHand, hoursRatio)
console.log(currentDate.getHours());

  if (currentDate.getHours()>= 18)
  {
  	 
     document.getElementById("clock").style.backgroundImage = "url('https://media.giphy.com/media/MXQnyEQwBJ6eTj90L5/giphy.gif')";
     
     var elements = document.getElementsByClassName('number'); // get all elements
  for(var i = 0; i < elements.length; i++){
    elements[i].style.color = "white";
  }
   var elements = document.getElementsByClassName('hand'); // get all elements
  for(var i = 0; i < elements.length; i++){
    elements[i].style.backgroundColor = "white";
  }
  	 }


  	 if (currentDate.getHours()< 5)
  {
  	  
  	 document.getElementById("clock").style.backgroundImage = "url('https://media.giphy.com/media/MXQnyEQwBJ6eTj90L5/giphy.gif')";
  	 
  	 var elements = document.getElementsByClassName('number'); // get all elements
	for(var i = 0; i < elements.length; i++){
		elements[i].style.color = "white";
	}
	 var elements = document.getElementsByClassName('hand'); // get all elements
	for(var i = 0; i < elements.length; i++){
		elements[i].style.backgroundColor = "white";
	}
}


  	  if (currentDate.getHours()>= 5 && currentDate.getHours()< 7)
  {
  	
  	 document.getElementById("clock").style.backgroundImage = "url('https://media.giphy.com/media/13DobtLzCTj16M/giphy.gif')";
  	 
  	 var elements = document.getElementsByClassName('number'); // get all elements
	for(var i = 0; i < elements.length; i++){
		elements[i].style.color = "black";
	}
	var elements = document.getElementsByClassName('hand'); // get all elements
    for(var i = 0; i < elements.length; i++){
        elements[i].style.backgroundColor = "black";
    }
  	 }



  	 if (currentDate.getHours()>= 7 && currentDate.getHours()< 17)
  {
  	  document.getElementById("clock").style.backgroundImage = "url('https://media.giphy.com/media/oBlpcDaHdWQY8/giphy.gif')";
     
     var elements = document.getElementsByClassName('number'); // get all elements
    for(var i = 0; i < elements.length; i++){
        elements[i].style.color = "black";
    }
     var elements = document.getElementsByClassName('hand'); // get all elements
    for(var i = 0; i < elements.length; i++){
        elements[i].style.backgroundColor = "black";
    }
  	 }

  	 if (currentDate.getHours()>= 17 && currentDate.getHours()< 18)
  {
  	  document.getElementById("clock").style.backgroundImage = "url('https://giphy.com/gifs/sun-gif-paMdZlRXlc96o')";
     
     var elements = document.getElementsByClassName('number'); // get all elements
    for(var i = 0; i < elements.length; i++){
        elements[i].style.color = "black";
    }
     var elements = document.getElementsByClassName('hand'); // get all elements
    for(var i = 0; i < elements.length; i++){
        elements[i].style.backgroundColor = "black";
    }
  	 }


}

function setRotation(element, rotationRatio) {
  element.style.setProperty('--rotation', rotationRatio * 360)
}

setClock()

function showTime(){
    var date = new Date();
    var h = date.getHours(); // 0 - 23
    var m = date.getMinutes(); // 0 - 59
    var s = date.getSeconds(); // 0 - 59
    var session = "AM";
    
    if(h == 0){
        h = 12;
    }
    
    if(h > 12){
        h = h - 12;
        session = "PM";
    }
    
    h = (h < 10) ? "0" + h : h;
    m = (m < 10) ? "0" + m : m;
    s = (s < 10) ? "0" + s : s;
    
    var time = h + ":" + m + ":" + s + " " + session;
    document.getElementById("MyClockDisplay").innerText = time;
    document.getElementById("MyClockDisplay").textContent = time;
    
    setTimeout(showTime, 1000);
    
}

showTime();